package com.antonioarce.langtalk;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.antonioarce.langtalk.Models.Dataset;
import com.antonioarce.langtalk.Models.Message;
import com.antonioarce.langtalk.Models.MessageDataset;
import com.antonioarce.langtalk.Models.User;
import com.claudiodegio.msv.MaterialSearchView;
import com.squareup.picasso.Picasso;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import io.codetail.animation.ViewAnimationUtils;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class ChatActivity extends AppCompatActivity implements
        MessageInput.AttachmentsListener, MessageInput.InputListener,
        MessagesListAdapter.SelectionListener,
        MessagesListAdapter.OnLoadMoreListener{

    private static final int TOTAL_MESSAGES_COUNT = 100;

    protected String senderId;
    protected ImageLoader imageLoader;
    protected MessagesListAdapter<Message> messagesAdapter;

    private Menu menu;
    private int selectionCount;
    private Date lastLoadedDate;

    private Toolbar mainChatToolbar;
    private MaterialSearchView materialChatSearchView;
    private MessagesList messagesList;
    private MessageInput inputChatMessage;
    CardView mRevealView;
    boolean hidden = true;
    Socket mSocket;
    ArrayList<Message> messages;
    User user;
    String senderSocketId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        mainChatToolbar = (Toolbar) findViewById(R.id.mainChatToolbar);
        materialChatSearchView = (MaterialSearchView) findViewById(R.id.materialChatSearchView);
        messagesList = (MessagesList) findViewById(R.id.messagesList);
        inputChatMessage = (MessageInput) findViewById(R.id.inputChatMessage);
        inputChatMessage.setAttachmentsListener(this);
        inputChatMessage.setInputListener(this);
        setSupportActionBar(mainChatToolbar);
        user = (User)getIntent().getSerializableExtra("user");
        senderId = user.getId();
        senderSocketId = getIntent().getStringExtra("senderSocketId");
        Toast.makeText(this, senderSocketId, Toast.LENGTH_SHORT).show();
        mRevealView = (CardView) findViewById(R.id.revealItems);
        mRevealView.setVisibility(View.INVISIBLE);
        initAdapter();
        initSocket();
    }

    private void initSocket() {
        mSocket = ((ChatApplication)getApplication()).getSocket();
        mSocket.on("onNewPrivateMessage", onNewMessage);

    }

    private void initAdapter(){
        imageLoader = new ImageLoader() {
            @Override
            public void loadImage(ImageView imageView, String url) {
                Picasso.get().load(url).into(imageView);
            }
        };

        messages = new ArrayList<>();
        lastLoadedDate = new Date();
        messagesAdapter = new MessagesListAdapter<>(senderId,imageLoader);
        messagesAdapter.addToEnd(messages, false);
        messagesList.setAdapter(messagesAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.toolbar_main_menu,menu);
        MenuItem item = menu.findItem(R.id.searchViewIcon);
        materialChatSearchView.setMenuItem(item);
        return true;
    }

    @Override
    public void onAddAttachments() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        if (hidden){
            int cx = mRevealView.getLeft();
            int cy = mRevealView.getBottom();

            // get the final radius for the clipping circle
            int dx = Math.max(cx, mRevealView.getWidth() - cx);
            int dy = Math.max(cy, mRevealView.getHeight() - cy);
            float finalRadius = (float) Math.hypot(dx, dy);
            mRevealView.setVisibility(View.VISIBLE);
            // Android native animator
            Animator animator =
                    ViewAnimationUtils.createCircularReveal(mRevealView, cx, cy, 0, finalRadius);
            animator.setInterpolator(new AccelerateDecelerateInterpolator());
            animator.setDuration(500);
            animator.start();
        }else{
            int cx = mRevealView.getLeft();
            int cy = mRevealView.getBottom();

            // get the final radius for the clipping circle
            int dx = Math.max(cx, mRevealView.getWidth() - cx);
            int dy = Math.max(cy, mRevealView.getHeight() - cy);
            float finalRadius = (float) Math.hypot(dx, dy);

            // Android native animator
            Animator animator =
                    ViewAnimationUtils.createCircularReveal(mRevealView, cx, cy, finalRadius, 0);
            animator.setInterpolator(new AccelerateDecelerateInterpolator());
            animator.setDuration(500);
            animator.start();
            animator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {}

                @Override
                public void onAnimationEnd(Animator animator) {
                    mRevealView.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onAnimationCancel(Animator animator) {}

                @Override
                public void onAnimationRepeat(Animator animator) {}
            });

        }
        hidden = !hidden;
    }

    public static void open(Context context, User user,String senderSocketId) {
        Intent i = new Intent(context, ChatActivity.class);
        i.putExtra("user",user);
        i.putExtra("senderSocketId",senderSocketId);
        context.startActivity(i);
    }

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Message object = new Message((JSONObject)args[0]);
                    messagesAdapter.addToStart(object, true);
                }
            });
        }
    };

    @Override
    public boolean onSubmit(CharSequence input) {
        Message message = new Message(Dataset.getRandomId(),user,input.toString());
        messagesAdapter.addToStart(message, true);
        try {
            JSONObject object = new JSONObject();
            object.put("socketId",senderSocketId);
            object.put("message", message.toJson());
            mSocket.emit("sendMessage", object);
        }catch (Exception e){

        }

        return true;
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {

    }

    @Override
    public void onSelectionChanged(int count) {

    }
}
