package com.antonioarce.langtalk;

import android.app.Application;
import android.util.Log;

import java.net.URISyntaxException;

import io.socket.client.Socket;
import io.socket.client.IO;

public class ChatApplication extends Application {

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("http://10.0.2.2:6677");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);

        }
    }

    public Socket getSocket() {
        return mSocket;
    }
}
