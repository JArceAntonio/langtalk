package com.antonioarce.langtalk;

import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.Toast;


import com.antonioarce.langtalk.Models.Dataset;
import com.antonioarce.langtalk.Models.Dialog;
import com.antonioarce.langtalk.Models.DialogDataset;
import com.antonioarce.langtalk.Models.Message;
import com.antonioarce.langtalk.Models.MessageDataset;
import com.antonioarce.langtalk.Models.User;
import com.antonioarce.langtalk.Utils.ContactsList;
import com.claudiodegio.msv.MaterialSearchView;
import com.squareup.picasso.Picasso;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.commons.models.IDialog;
import com.stfalcon.chatkit.dialogs.DialogsList;
import com.stfalcon.chatkit.dialogs.DialogsListAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class MainActivity extends AppCompatActivity implements DialogsListAdapter.OnDialogClickListener, DialogsListAdapter.OnDialogLongClickListener{

    private Toolbar mainToolbar;
    private MaterialSearchView materialSearchView;
    private DialogsList dialogsList;

    private ImageLoader imageLoader;
    private List<Dialog> contactList;
    private DialogsListAdapter dialogsListAdapter;
    Vibrator vibrator;
    private Socket mSocket;
    User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainToolbar = (Toolbar) findViewById(R.id.mainToolbar);
        materialSearchView = (MaterialSearchView) findViewById(R.id.materialSearchView);
        dialogsList = (DialogsList) findViewById(R.id.dialogsList);
        setSupportActionBar(mainToolbar);
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        user = new User(Dataset.getRandomId(),Dataset.getRandomName(),Dataset.getRandomAvatar(),Dataset.getRandomBoolean());
        initAdapter();
        initSocket();
    }

    private void initSocket() {
        ChatApplication app = (ChatApplication) getApplication();
        mSocket = app.getSocket();
        mSocket.on("newUserConnected", onNewUserConnected)
        .on("onConnect",onConnect)
        .on("onDisconnect", onDisconnect)
        .on("newUser", onNewUser)
        .on("getUsers", getUsers);
        mSocket.connect();
        mSocket.emit("onConnect",user.toJson());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_main_menu,menu);
        MenuItem searchItem = menu.findItem(R.id.searchViewIcon);
        materialSearchView.setMenuItem(searchItem);
        return true;
    }

    @Override
    public void onDialogClick(IDialog dialog) {
        ChatActivity.open(this, user, ((User)dialog.getUsers().get(0)).getSocketId());
        //mSocket.emit("onNewMessage","Message from android!!");
    }

    @Override
    public void onDialogLongClick(IDialog dialog) {
        Toast.makeText(this, "Dialog OnLongClick", Toast.LENGTH_SHORT).show();
        if (vibrator.hasVibrator()) {
            vibrator.vibrate(500);
        }
    }

    private void initAdapter(){
        imageLoader = new ImageLoader() {
            @Override
            public void loadImage(ImageView imageView, String url) {
                Picasso.get().load(url).into(imageView);
            }
        };
        dialogsListAdapter = new DialogsListAdapter(imageLoader);
        contactList = new ArrayList<>();
        dialogsListAdapter.setItems(contactList);
        dialogsListAdapter.setOnDialogClickListener(this);
        dialogsListAdapter.setOnDialogLongClickListener(this);
        dialogsList.setAdapter(dialogsListAdapter);
    }


    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

        }
    };

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

        }
    };

    private Emitter.Listener onNewUserConnected = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

        }
    };

    private Emitter.Listener onNewUser = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    User userConnected = new User((JSONObject) args[0]);
                    String socketId = (String) args[1];
                    if (user.getId().equals(userConnected.getId())){
                        user.setSocketId(socketId);
                        Log.d("SocketId", socketId);

                    }else{
                        ArrayList<User> users= new ArrayList<>();
                        users.add(userConnected);
                        contactList.add(new Dialog(Dataset.getRandomId(),userConnected.getName(),userConnected.getAvatar(),users, Message.emptyMessage(userConnected),0));
                        dialogsListAdapter.notifyItemInserted(contactList.size() - 1);
                        Log.d("DatasetChanged", "dataset has been changed");
                    }
//            Log.d("onNewUser",((JSONObject) args[0]).toString());
//            Log.d("onNewUser2",(String) args[1]);
                }
            });
        }
    };

    private Emitter.Listener getUsers = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONArray usersJson = (JSONArray) args[0];
                    try{

                        for(int i = 0; i < usersJson.length(); i++){
                            ArrayList<User> users = new ArrayList<>();
                            User userConnected = new User(usersJson.getJSONObject(i));
                            users.add(userConnected);
                            contactList.add(new Dialog(Dataset.getRandomId(),userConnected.getName(),userConnected.getAvatar(),users, Message.emptyMessage(userConnected),0));
                            dialogsListAdapter.notifyItemInserted(contactList.size() - 1);
                        }
                    }catch (Exception e){

                    }
                    Toast.makeText(MainActivity.this, "getUsers performed  " + Integer.toString(usersJson.length()), Toast.LENGTH_SHORT).show();

                }
            });
        }
    };
}
