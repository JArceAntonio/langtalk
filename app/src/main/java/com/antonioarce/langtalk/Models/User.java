package com.antonioarce.langtalk.Models;

import com.stfalcon.chatkit.commons.models.IUser;

import org.json.JSONObject;

import java.io.Serializable;

/*
 * Created by troy379 on 04.04.17.
 */
public class User implements IUser, Serializable {

    private String id;
    private String name;
    private String avatar;
    private boolean online;
    private String socketId;

    public User(String id, String name, String avatar, boolean online) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        this.online = online;
        socketId = "";
    }

    public User(JSONObject jsonObject) {
        try {
            this.id = jsonObject.getString("id");
            this.name = jsonObject.getString("name");
            this.avatar = jsonObject.getString("avatar");
            this.online = jsonObject.getBoolean("online");
            this.socketId = jsonObject.getString("socketId");
        }catch (Exception e){

        }
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getAvatar() {
        return avatar;
    }

    public boolean isOnline() {
        return online;
    }

    public JSONObject toJson(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id",id);
            jsonObject.put("name",name);
            jsonObject.put("avatar",avatar);
            jsonObject.put("online",online);
            jsonObject.put("socketId",socketId);
        }catch (Exception e){
            return null;
        }
        return jsonObject;
    }

    public String getSocketId() {
        return socketId;
    }

    public void setSocketId(String socketId) {
        this.socketId = socketId;
    }
}
